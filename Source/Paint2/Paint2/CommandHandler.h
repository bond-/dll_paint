class CCommandHandler : public IUICommandHandler
{
public:
	static HRESULT CreateInstance(IUICommandHandler **ppCommandHandler);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();
	STDMETHODIMP QueryInterface(REFIID iid, void** ppv);
	STDMETHOD(UpdateProperty)(UINT nCmdID,
		REFPROPERTYKEY key,
		const PROPVARIANT* ppropvarCurrentValue,
		PROPVARIANT* ppropvarNewValue);
	STDMETHOD(Execute)(UINT nCmdID,
		UI_EXECUTIONVERB verb,
		const PROPERTYKEY* key,
		const PROPVARIANT* ppropvarValue,
		IUISimplePropertySet* pCommandExecutionProperties);
private:
	CCommandHandler() : m_cRef(1) {}
	LONG m_cRef;
};

#include "stdafx.h"
#include "RibbonFramework.h"
#include "Application.h"

IUIFramework *g_pFramework = NULL; 
IUIApplication *g_pApplication = NULL;
bool InitializeFramework(HWND hWnd)
{
	HRESULT hr = CoCreateInstance(CLSID_UIRibbonFramework, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&g_pFramework));
	if (FAILED(hr))
	{
		return false;
	}

	hr = CApplication::CreateInstance(&g_pApplication);
	if (FAILED(hr))
	{
		return false;
	}

	hr = g_pFramework->Initialize(hWnd, g_pApplication);
	if (FAILED(hr))
	{
		return false;
	}
	hr = g_pFramework->LoadUI(GetModuleHandle(NULL), L"APPLICATION_RIBBON");
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}
void DestroyFramework()
{
	if (g_pFramework)
	{
		g_pFramework->Destroy();
		g_pFramework->Release();
		g_pFramework = NULL;
	}

	if (g_pApplication)
	{
		g_pApplication->Release();
		g_pApplication = NULL;
	}
}

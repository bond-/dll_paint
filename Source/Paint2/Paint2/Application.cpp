#include <UIRibbon.h>
#include <UIRibbonPropertyHelpers.h>
#include "Application.h"
#include "CommandHandler.h"

HRESULT CApplication::CreateInstance(IUIApplication **ppApplication)
{
	*ppApplication = NULL;

	HRESULT hr = S_OK;

	CApplication* pApplication = new CApplication();

	if (pApplication != NULL)
	{
		*ppApplication = static_cast<IUIApplication *>(pApplication);
	}
	else
	{
		hr = E_OUTOFMEMORY;
	}

	return hr;
}
STDMETHODIMP_(ULONG) CApplication::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

STDMETHODIMP_(ULONG) CApplication::Release()
{
	LONG cRef = InterlockedDecrement(&m_cRef);
	if (cRef == 0)
	{
		delete this;
	}

	return cRef;
}

STDMETHODIMP CApplication::QueryInterface(REFIID iid, void** ppv)
{
	if (iid == __uuidof(IUnknown))
	{
		*ppv = static_cast<IUnknown*>(this);
	}
	else if (iid == __uuidof(IUIApplication))
	{
		*ppv = static_cast<IUIApplication*>(this);
	}
	else
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	AddRef();
	return S_OK;
}
STDMETHODIMP CApplication::OnCreateUICommand(
	UINT nCmdID,
	UI_COMMANDTYPE typeID,
	IUICommandHandler** ppCommandHandler)
{
	UNREFERENCED_PARAMETER(typeID);
	UNREFERENCED_PARAMETER(nCmdID);

	if (NULL == m_pCommandHandler)
	{
		HRESULT hr = CCommandHandler::CreateInstance(&m_pCommandHandler);
		if (FAILED(hr))
		{
			return hr;
		}
	}

	return m_pCommandHandler->QueryInterface(IID_PPV_ARGS(ppCommandHandler));
}

STDMETHODIMP CApplication::OnViewChanged(
	UINT viewId,
	UI_VIEWTYPE typeId,
	IUnknown* pView,
	UI_VIEWVERB verb,
	INT uReasonCode)
{
	UNREFERENCED_PARAMETER(uReasonCode);
	UNREFERENCED_PARAMETER(viewId);

	HRESULT hr = E_NOTIMPL;
	if (UI_VIEWTYPE_RIBBON == typeId)
	{
		switch (verb)
		{
		case UI_VIEWVERB_CREATE:
			hr = S_OK;
			break;
		case UI_VIEWVERB_DESTROY:
			hr = S_OK;
			break;
		}
	}

	return hr;
}
STDMETHODIMP CApplication::OnDestroyUICommand(
	UINT32 nCmdID,
	UI_COMMANDTYPE typeID,
	IUICommandHandler* commandHandler)
{
	UNREFERENCED_PARAMETER(commandHandler);
	UNREFERENCED_PARAMETER(typeID);
	UNREFERENCED_PARAMETER(nCmdID);

	return E_NOTIMPL;
}

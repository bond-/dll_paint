#pragma once

#define MATHLIBLIBRARY_API _declspec(dllexport)

#include <windowsX.h>
using namespace std;
#include <vector>
#include <Objbase.h>
#include <objidl.h>
#include <gdiplus.h>
#pragma comment(lib, "Ole32.lib")
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

namespace DllLibrary
{
	class MATHLIBLIBRARY_API CShape
	{
	public:
		POINT start, last;
		COLORREF sColor = RGB(0, 0, 0);
		int sSize = 1;
		virtual void Draw(HDC hdc) = 0;
		virtual CShape* Create() = 0;
		virtual void SetData(int a, int b, int c, int d) = 0;
	};

	class MATHLIBLIBRARY_API CLine :public CShape
	{
	public:

		void draw(Graphics * grap)
		{
			grap->SetData(start.x, start.y, last.x, last.y);
		}
	};

	class MATHLIBLIBRARY_API CRect :public CShape
	{
	public:

		void draw(Graphics * grap)
		{
			grap->DrawRectangle(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}
	};

	class MATHLIBLIBRARY_API CEllipse : public CShape
	{
	public:

		void draw(Graphics *grap)
		{
			grap->DrawEllipse(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}

	};

	void MATHLIBLIBRARY_API ChagetoSpecial(Point &start, Point &last)
	{
		if (abs(start.X - last.X) <  abs(start.Y - last.Y))
		{
			if (start.Y < last.Y)
				last.Y = start.Y + abs(start.X - last.X);
			else last.Y = start.Y - abs(start.X - last.X);
		}
		else
		{
			if (start.X < last.X)
				last.X = start.X + abs(start.Y - last.Y);
			else last.X = start.X - abs(start.Y - last.Y);
		}
	}

	class MATHLIBLIBRARY_API CSquare :public CShape
	{
	public:
		void draw(Graphics *grap)
		{
			ChagetoSpecial(start, last);
			grap->DrawRectangle(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}
	};

	class MATHLIBLIBRARY_API CRound : public CShape
	{
	public:
		void draw(Graphics *grap)
		{
			ChagetoSpecial(start, last);
			grap->DrawEllipse(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}
	};
}